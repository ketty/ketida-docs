#!/bin/bash -x

set -x

# node_modules/.bin/start-storybook \
#   --port 5000 \ 
#   --config-dir node_modules/@coko/storybook/src/config \
#   --docs



node_modules/.bin/start-storybook \
  --docs \
  --port ${STORYBOOK_PORT:-5000} \
  --config-dir node_modules/@coko/storybook/src/config
