import React from 'react'

const Image = props => {
  const { maxWidth, ...rest } = props

  return (
    <img
      style={{
        maxWidth,
      }}
      {...rest}
    />
  )
}

export default Image
