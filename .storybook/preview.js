/* eslint-disable import/prefer-default-export */

export const parameters = {
  options: {
    storySort: {
      order: [
        'Home',
        'Requirements of Editoria',
        'Quick Start',
        'Deployment',
        ['Introduction', 'Services', 'Object Storage', 'Editoria'],
        'Configuration',
        ['Scripts'],
      ],
    },
  },
}
